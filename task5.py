import csv
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import time

#loading data form matches.csv file in a list
def load_matches_data():
    matches = []
    with open ('matches.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for data in reader:
            matches.append(data)
        return matches


def task5():
    matches = load_matches_data()
    matches_won_per_team = {}
    for i in range(len(matches)):
        if matches[i]['result']!='no result' and matches[i]['winner']=='Sunrisers Hyderabad' or matches[i]['winner']=='Mumbai Indians' or matches[i]['winner']=='Kolkata Knight Riders' or matches[i]['winner']=='Chennai Super Kings':
            winner = matches[i]['winner']
            season = matches[i]['season']
            if winner in matches_won_per_team:
                pass
            else:
                matches_won_per_team[winner]={}
            if season in matches_won_per_team[winner]:
                matches_won_per_team[winner][season]+=1
            else:
                matches_won_per_team[winner][season]=1
    return matches_won_per_team

start_time = time.time()
data = task5()

teams = list(data.keys())

t=[]
for team in teams:
    y_offset = 0
    teams_data = data[team]
    years = (teams_data.keys())
    for year in years:
        p=plt.bar(team,teams_data[year], bottom=y_offset)
        t.append(p)
        y_offset+= teams_data[year]

plt.legend(t,list(data[team]))


end_time = time.time()-start_time
print(end_time)
plt.show()




