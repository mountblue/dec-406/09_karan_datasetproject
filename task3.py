import csv
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import time

#loading data form matches.csv file in a list
def load_matches_data():
    matches = []
    with open ('matches.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for data in reader:
            matches.append(data)
        return matches


#loading data form deliveries.csv file in a list
def load_deliveries_data():
    deliveries = []
    with open ('deliveries.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for data in reader:
            deliveries.append(data)
        return deliveries


def task3():
    matches = load_matches_data()
    deliveries = load_deliveries_data()
    matchId = []
    extra_runs = {}
    for i in range(len(matches)):
        if matches[i]['season']== '2016':
            matchId.append(matches[i]['id'])
    for x in range(len(deliveries)):
        matchid = deliveries[x]['match_id']
        if matchid in matchId:
            team = deliveries[x]['bowling_team']
            if team in extra_runs:
                extra_runs[team]+=int(deliveries[x]['extra_runs']) 
            else:
                extra_runs[team]= int(deliveries[x]['extra_runs'])
    return extra_runs

#plotting in graph

start_time = time.time()
data = task3()

group_data = list(data.values())
group_names = list(data.keys())
x_axis = list(range(len(group_names)))
plt.bar(x_axis, group_data)
plt.xticks(x_axis, group_names, rotation=90)
plt.tight_layout()

end_time = time.time()-start_time
print(end_time)
plt.show()