import csv
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import time


#loading data form matches.csv file in a list
def load_matches_data():
    matches = []
    with open ('matches.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for data in reader:
            matches.append(data)
        return matches

start_time = time.time()

def task1():
    matches = load_matches_data()
    matchesPerYear = {}
    for i in range(len(matches)):
        season = matches[i]['season']
        if season in matchesPerYear:
            matchesPerYear[season]+=1
        else:
            matchesPerYear[season] = 1
    return matchesPerYear

data = task1()

#plotting in graph

group_data = list(data.values())
group_names = list(data.keys())


plt.bar(group_names, group_data)
end_time = time.time()-start_time
print(end_time)
plt.show()
