import csv 
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import time


#load matches data 
def load_matches_data():
    matches = []
    with open ('matches.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for data in reader:
            matches.append(data)
        return matches


#loading data form deliveries.csv file in a list
def load_deliveries_data():
    deliveries = []
    with open ('deliveries.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for data in reader:
            deliveries.append(data)
        return deliveries

start_time = time.time()
def task4():
    matches = load_matches_data()
    deliveries = load_deliveries_data()
    matchId = []
    economical_bowlers = {}
    bowler_bowl = {}
    for i in range(len(matches)):
        if matches[i]['season']== '2015':
            matchId.append(matches[i]['id'])
    for x in range(len(deliveries)):
        matchid = deliveries[x]['match_id']
        if matchid in matchId:
            bowler = deliveries[x]['bowler']
            if bowler in economical_bowlers:
                economical_bowlers[bowler]+=  (int(deliveries[x]['total_runs'])-int(deliveries[x]['bye_runs'])) 
                bowler_bowl[bowler]+=1
            else:
                economical_bowlers[bowler]= (int(deliveries[x]['total_runs'])-int(deliveries[x]['bye_runs']))
                bowler_bowl[bowler] = 1
    for key in economical_bowlers:
        economical_bowlers[key] = round(economical_bowlers[key]*600/bowler_bowl[key])/100
    
    most_economical_bowlers = {}
    for key in economical_bowlers:
        if economical_bowlers[key]<=7:
            most_economical_bowlers[key]= economical_bowlers[key]
    return most_economical_bowlers


#plotting in graph
data = task4()

group_data = list(data.values())
group_names = list(data.keys())
x_axis = list(range(len(group_names)))

plt.bar(group_names, group_data)
plt.xticks(x_axis, group_names, rotation=90)
plt.tight_layout()

end_time = time.time()-start_time
print(end_time)
plt.show()