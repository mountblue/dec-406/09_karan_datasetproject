import csv
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import time

#loading data form matches.csv file in a list
def load_matches_data():
    matches = []
    with open ('matches.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for data in reader:
            matches.append(data)
        return matches

def task2():
    matches = load_matches_data()
    matchesWonPerTeam = {}
    for i in range(len(matches)):
        winner = matches[i]['winner']
        season = matches[i]['season']
        if winner!='':
            if season in matchesWonPerTeam:
                pass
            elif season not in matchesWonPerTeam and matches[i]['result']=='normal':
                matchesWonPerTeam[season] = {}
                matchesWonPerTeam[season][winner]=1
            if winner in matchesWonPerTeam[season]:
                matchesWonPerTeam[season][winner]+=1
            else:
                matchesWonPerTeam[season][winner]=1
    return matchesWonPerTeam


startTime = time.time()

data = task2()

years = list(data.keys())
# years_data = list(data.values())

teams = ['Sunrisers Hyderabad', 'Rising Pune Supergiant', 'Kolkata Knight Riders', 'Kings XI Punjab', 'Royal Challengers Bangalore', 'Mumbai Indians', 'Delhi Daredevils', 'Gujarat Lions', 'Chennai Super Kings', 'Rajasthan Royals', 'Deccan Chargers', 'Pune Warriors', 'Kochi Tuskers Kerala', 'Rising Pune Supergiants']
for year in years:
    for team in teams:
        if team not in data[year].keys():
            data[year][team]=0



color_set = ['#4E79A7', '#F28E2B', '#E15759', '#76B7B2',
             '#59A14F', '#EDC948', '#B07AA1', '#FF9D17',
             '#4E79A7', '#F28E2B', '#E15759', '#76B7B2',
             '#59A14F', '#EDC948', '#B07AA1', '#FF9D17',]


p = []
color_start = 0
for year in years:
    color_start = 0
    y_offset = 0
    years_data = data[year]
    for team in teams:
        t=plt.bar(year,years_data[team], bottom=y_offset, color=color_set[color_start])
        color_start += 1
        p.append(t)
        y_offset+= years_data[team]
 

plt.legend(p,teams)
plt.tight_layout()

end_time = time.time()-startTime
print(end_time)
plt.show()
    

